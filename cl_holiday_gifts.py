"""
Module for find_gift-cost function

Author: WilliamA
Date: August 2021


"""

import numpy as np


def find_gift_costs(file_path):
    """
    Args: Path File containing the gift costs

    Output: Gift costs

    """
    with open(file_path) as file:
        gift_costs = file.read().split('\n')
    gift_costs = np.array(gift_costs).astype(int)
    total_price = (gift_costs[gift_costs < 25]).sum() * 1.08

    return total_price


if __name__ == "__main__":
    GIFT_COSTS = find_gift_costs('gift_costs.txt')
    print(GIFT_COSTS)
